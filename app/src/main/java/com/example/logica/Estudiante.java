package com.example.logica;

public class Estudiante {//object value
    public String id;
    public String nombre;
    public String apellido;
    public String correo;

    public Estudiante(){

    }

    public Estudiante(String id, String nombre, String apellido, String correo) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
    }
}
